<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Login - Make Switch</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
  <link rel="stylesheet" href="../src/css/login.css">
</head>

<body>
  <div class="main d-flex align-items-center justify-content-center">
    <div class="content">
      <div class="row">
        <div class="col-md-8 img-content d-flex align-items-center justify-content-center">
          <div class="border-title">
            <h2>Find the essential</h2>
          </div>
        </div>
        <div class="col-md-4 d-flex flex-column align-items-center justify-content-center">
          <div class="logo">
            <img src="../src/img/Capture.png" alt="" srcset="">
          </div>
          <form class="form-style-login">
            <div class="form-group form-login">
              <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
            </div>
            <div class="form-group form-login">
              <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
              <small><a href="#">ESQUECI MINHA SENHA</a></small>
            </div>
            <button type="submit" class="btn btn-login">SUBMIT</button>
            <div class="row d-flex align-items-center">
              <div class="col-md-5 line">
                <hr>
              </div>
              <div class="col-md-2 or-login">
                <span>or</span>
              </div>
              <div class="col-md-5 line">
                <hr>
              </div>
            </div>
            <a href="#" class="btn btn-login">CREATE ACOUNT</a>
          </form>
        </div>
      </div>
    </div>
  </div>

  <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>

</html>